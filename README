Hello, welcome, gather round!

Glentoo is a Google Summer of Code 2011 project maintained by yours truly, Robert Seaton, and it is one of many projects that fall under the scope of the Gentoo Foundation. The idea behind Glentoo is to create a Gentoo-based liveCD with a twist. Instead of the typical GNU userspace, Glentoo will have a Plan 9 inspired userspace, building on the work done by the Glendix and Plan 9 From Userspace projects. 

The goals of Glentoo are two-fold:

1) Linux, by virtue of its popularity, has broad hardware support, while Plan 9 From Bell Labs has very limited hardware support. By exchanging the Plan 9 kernel with a Linux kernel, Glentoo will be able to bring Plan 9 to a much wider audience. By introducing the experimental concepts from Plan 9 to more people, I hope to encourage the proliferation of new ideas.

Plan 9 from Bell Labs has a lot of exciting ideas, some of which have already been adopted by the GNU/Linux crowd. Linux's /proc filesystem, for example, was inspired Plan 9's everything-is-a-file mentality. Further, Plan 9 From Bell Labs was the first operating system with support for unicode, and I talked to a Plan 9-er who told me that he was working with Plan 9 at IBM because its design allows for very efficient communication between VMs. So, those are just a couple of examples of how cool things from Plan 9 are already being used and I hope that more ideas from Plan 9 will spread in the future with the help of Glentoo.

2) Hopefully, Glentoo will serve not only as an introduction to ideas from Plan 9, but also show the world all of the awesome things that can be done with Gentoo. Glentoo, ideally, will serve as a sort of gateway to get people interested in Gentoo and the OSS community.
