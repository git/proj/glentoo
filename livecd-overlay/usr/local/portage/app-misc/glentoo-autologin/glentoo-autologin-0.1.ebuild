# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

inherit eutils

DESCRIPTION="Handle automatically logging in on system boot."
LICENSE="GPL-2"
KEYWORDS="x86"

src_install() {
              doinitd autologin
}